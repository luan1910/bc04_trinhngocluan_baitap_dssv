const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

// chức năng thêm sinh viên
var dssv = [];
// lấy thông tin từ localStorage
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  // array khi covert thành json sẽ mất function, ta sẽ map lại
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.matKhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }
  renderDSSV(dssv);
}
function themSV() {
  var newSv = layThongTinTuForm();
  //   console.log("newSv: ", newSv);
  // kiểm tra mã SV
  var isValid =
    validation.kiemTraRong(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên không được rỗng"
    ) &&
    validation.kiemTraDoDai(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên phải gồm 4 kí tự",
      4,
      4
    );
  // kiểm tra tên SV
  isValid =
    isValid &
    validation.kiemTraRong(
      newSv.ten,
      "spanTenSV",
      "Tên sinh viên không được rỗng"
    );
  // kiểm tra email SV
  isValid =
    isValid &
    validation.kiemTraRong(
      newSv.email,
      "spanEmailSV",
      "Email không được để rỗng"
    );
  validation.kiemTraEmail(newSv.email, "spanEmailSV", "Email không hợp lệ");
  if (isValid) {
    dssv.push(newSv);

    // tạo json
    var dssvJson = JSON.stringify(dssv);
    // lưu json vào localStorage
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);

    renderDSSV(dssv);
  }
}

// render array sv ra giao diện
function xoaSinhVien(id) {
  console.log(id);
  // var index = dssv.findIndex(function (sv) {
  //   return sv.ma == id;
  // });
  // console.log("index: ", index);
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  dssv.splice(index, 1);
  renderDSSV(dssv);
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
}
