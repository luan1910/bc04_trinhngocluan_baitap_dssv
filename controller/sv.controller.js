function layThongTinTuForm() {
  const maSv = document.getElementById("txtMaSV").value;
  const tenSv = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matKhau = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value * 1;
  const diemLy = document.getElementById("txtDiemLy").value * 1;
  const diemHoa = document.getElementById("txtDiemHoa").value * 1;

  return new SinhVien(tenSv, maSv, matKhau, email, diemToan, diemLy, diemHoa);
}

// render array sv ra giao diện
function renderDSSV(svArr) {
  console.log("svArr: ", svArr);
  var contentHTML = "";
  for (var i = 0; i < svArr.length; i++) {
    var sv = svArr[i];
    // trContent thẻ tr trong mỗi lần lặp
    var trContent = `<tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>${sv.tinhDTB()}</td>
    <td>
    <button onclick="xoaSinhVien('${sv.ma}')" class="btn btn-danger">Xóa</button>
    <button onclick="suaSinhVien('${sv.ma}')" class="btn btn-warning">Sửa</button>
    </td>
    </tr>`;
    contentHTML += trContent;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

// render array sv ra giao diện
// function xoaSinhVien(id) {
//   console.log(id);
//   var index = dssv.findIndex(function (sv) {
//     return sv.ma == id;
//   });
//   console.log("index: ", index);
// }

function timKiemViTri(id,dssv) { 
  for (var index=0; index <dssv.length; index++){
    var sv = dssv[index]
    if (sv.ma == id) { 

      return index;}
  }
  console.log(index);
  return -1;
}

function showThongTinLenForm(sv) {
document.getElementById("txtMaSV").value = sv.ma;
document.getElementById("txtTenSV").value = sv.ten;
document.getElementById("txtEmail").value = sv.email;
document.getElementById("txtPass").value = sv.matKhau;
document.getElementById("txtDiemToan").value = sv.toan;
document.getElementById("txtDiemLy").value = sv.ly;
document.getElementById("txtDiemHoa").value = sv.hoa;

}