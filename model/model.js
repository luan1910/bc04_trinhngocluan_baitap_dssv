function SinhVien(ten, ma, matKhau, email, toan, ly, hoa) {
  this.ten = ten;
  this.ma = ma;
  this.matKhau = matKhau;
  this.email = email;
  this.toan = toan;
  this.ly = ly;
  this.hoa = hoa;
  this.tinhDTB = function () {
    return (this.toan + this.ly + this.hoa) / 3;
  };
}
